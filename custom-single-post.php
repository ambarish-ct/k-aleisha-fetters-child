<?php
/**
 * Template Name: Custom Single Post Page
 *
 * @package Wordpress
 * @since   2.3
 */
get_header();

$breadcrumb = get_post_meta( get_the_ID(), 'penci_page_breadcrumb', true );

$featured_boxes = get_post_meta( get_the_ID(), 'penci_page_display_featured_boxes', true );

$page_meta = get_post_meta( get_the_ID(), 'penci_page_slider', true );

$rev_shortcode = get_post_meta( get_the_ID(), 'penci_page_rev_shortcode', true );



if( in_array( $page_meta, array('style-1', 'style-2', 'style-3', 'style-4', 'style-5', 'style-6', 'style-7', 'style-8', 'style-9', 'style-10', 'style-11', 'style-12', 'style-13', 'style-14', 'style-15', 'style-16', 'style-17', 'style-18', 'style-19', 'style-20', 'style-21', 'style-22', 'style-23', 'style-24', 'style-25',	'style-26', 'style-27', 'style-28', 'style-29', 'style-30',	'style-31', 'style-32', 'style-33', 'style-34', 'style-35', 'style-36', 'style-37', 'style-38', 'video' ) ) ) {

	if( $page_meta == 'video' && get_theme_mod( 'penci_featured_video_url' ) ) {

		get_template_part( 'inc/featured_slider/featured_video' );

	} else {

		if( ( $page_meta == 'style-33' || $page_meta == 'style-34' ) && $rev_shortcode ) {

			echo '<div class="featured-area featured-' . $page_meta . '">';

			if( $page_meta == 'style-34' ): echo '<div class="container">'; endif;

			echo do_shortcode( $rev_shortcode );

			if( $page_meta == 'style-34' ): echo '</div>'; endif;

			echo '</div>';

		} else {

			if ( get_theme_mod( 'penci_body_boxed_layout' ) && ! get_theme_mod( 'penci_vertical_nav_show' ) ) {

				if( $page_meta == 'style-3' ) {

					$page_meta == 'style-1';

				} elseif( $page_meta == 'style-5' ) {

					$page_meta == 'style-4';

				} elseif( $page_meta == 'style-7' ) {

					$page_meta == 'style-8';

				} elseif( $page_meta == 'style-9' ) {

					$page_meta == 'style-10';

				} elseif( $page_meta == 'style-11' ) {

					$page_meta == 'style-12';

				} elseif( $page_meta == 'style-13' ) {

					$page_meta == 'style-14';

				} elseif( $page_meta == 'style-15' ) {

					$page_meta == 'style-16';

				} elseif( $page_meta == 'style-17' ) {

					$page_meta == 'style-18';

				} elseif( $page_meta == 'style-29' ) {

					$page_meta == 'style-30';

				} elseif( $page_meta == 'style-35' ) {

					$page_meta == 'style-36';

				}

			}

			$slider_class = $page_meta;

			if( $page_meta == 'style-5' ) {

				$slider_class = 'style-4 style-5';

			} elseif ( $page_meta == 'style-30' ) {

				$slider_class = 'style-29 style-30';

			} elseif ( $page_meta == 'style-36' ) {

				$slider_class = 'style-35 style-36';

			}

			$data_auto = 'false';

			$data_loop = 'true';

			$data_res = '';



			if( $page_meta == 'style-7' || $page_meta == 'style-8' ){

				$data_res = ' data-item="4" data-desktop="4" data-tablet="2" data-tabsmall="1"';

			} elseif( $page_meta == 'style-9' || $page_meta == 'style-10' ){

				$data_res = ' data-item="3" data-desktop="3" data-tablet="2" data-tabsmall="1"';

			} elseif( $page_meta == 'style-11' || $page_meta == 'style-12' ){

				$data_res = ' data-item="2" data-desktop="2" data-tablet="2" data-tabsmall="1"';

			} elseif( in_array( $page_meta, array( 'style-31', 'style-32', 'style-35', 'style-36', 'style-37' ) ) ) {

				$data_next_prev = get_theme_mod( 'penci_enable_next_prev_penci_slider' ) ? 'true' : 'false';

				$data_dots = get_theme_mod( 'penci_disable_dots_penci_slider' ) ? 'false' : 'true';

				$data_res = ' data-dots="'. $data_dots .'" data-nav="'. $data_next_prev .'"';

			}



			if( get_theme_mod( 'penci_featured_autoplay' ) ): $data_auto = 'true'; endif;

			if( get_theme_mod( 'penci_featured_loop' ) ): $data_loop = 'false'; endif;

			$auto_time = get_theme_mod( 'penci_featured_slider_auto_time' );

			if( !is_numeric( $auto_time ) ): $auto_time = '4000'; endif;

			$auto_speed = get_theme_mod( 'penci_featured_slider_auto_speed' );

			if( !is_numeric( $auto_speed ) ): $auto_speed = '600'; endif;

			$open_container = '';

			$close_container = '';

			if( in_array( $page_meta, array( 'style-1', 'style-4', 'style-6', 'style-8', 'style-10', 'style-12', 'style-14', 'style-16', 'style-18', 'style-19', 'style-20', 'style-21', 'style-22', 'style-23', 'style-24', 'style-25', 'style-26', 'style-27', 'style-30', 'style-32', 'style-36', 'style-37' ) ) ):

				$open_container = '<div class="container">';

				$close_container = '</div>';

			endif;

			

			if( get_theme_mod( 'penci_enable_flat_overlay' ) && in_array( $page_meta, array( 'style-6', 'style-7', 'style-8', 'style-9', 'style-10', 'style-11', 'style-12', 'style-13', 'style-14', 'style-15', 'style-16', 'style-17', 'style-18', 'style-19', 'style-20', 'style-21', 'style-22', 'style-23', 'style-24', 'style-25', 'style-26', 'style-27', 'style-28' ) ) ): $slider_class .= ' penci-flat-overlay'; endif;



			echo '<div class="featured-area featured-' . $slider_class . '">' . $open_container;

			if( $page_meta == 'style-37' ):

				echo '<div class="penci-featured-items-left">';

			endif;

			echo '<div class="penci-owl-carousel penci-owl-featured-area"'. $data_res .' data-style="'. $page_meta .'" data-auto="'. $data_auto .'" data-autotime="'. $auto_time .'" data-speed="'. $auto_speed .'" data-loop="'. $data_loop .'">';

			get_template_part( 'inc/featured_slider/' . $page_meta );

			echo '</div>';

			echo $close_container. '</div>';

		}

	}

}



/* Display Featured Boxes */

if ( $featured_boxes == 'yes' && ! get_theme_mod( 'penci_home_hide_boxes' ) && ( get_theme_mod( 'penci_home_box_img1' ) || get_theme_mod( 'penci_home_box_img2' ) || get_theme_mod( 'penci_home_box_img3' ) || get_theme_mod( 'penci_home_box_img4' ) ) ):

	get_template_part( 'inc/modules/home_boxes' );

endif;



?>

<?php $show_page_title = penci_is_pageheader(); ?>

<?php if( ! $show_page_title ): ?>

	<?php if ( ! get_theme_mod( 'penci_disable_breadcrumb' ) && ( 'no' != $breadcrumb ) ): ?>

		<?php

		$yoast_breadcrumb = '';

		if ( function_exists( 'yoast_breadcrumb' ) ) {

			$yoast_breadcrumb = yoast_breadcrumb( '<div class="container container-single-page penci-breadcrumb">', '</div>', false );

		}



		if( $yoast_breadcrumb ){

			echo $yoast_breadcrumb;

		}else{ ?>

		<div class="container container-single-page penci-breadcrumb">

			<span><a class="crumb" href="<?php echo esc_url( home_url('/') ); ?>"><?php echo penci_get_setting( 'penci_trans_home' ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>

			<?php

			$page_parent = get_post_ancestors( get_the_ID() );

			if( ! empty( $page_parent ) ):

				$page_parent = array_reverse($page_parent);

				foreach( $page_parent as $pages ){

			?>

				<span><a class="crumb" href="<?php echo get_permalink( $pages ); ?>"><?php echo get_the_title( $pages ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>

			<?php }

			endif; ?>

			<span><?php the_title(); ?></span>

		</div>

		<?php } ?>

	<?php endif; ?>

<?php endif; ?>



	<div class="template-single-post container-single-page else penci_is_nosidebar container-single-page-full-width">
	<?php 
		$page_id 								=	get_the_ID(); 
		$page_fields  						=  get_fields($page_id);
		$banner_section  				=	$page_fields['banner_section'];
		$content_section  				=  $page_fields['content_section'];
		$text_image_section  		=  $page_fields['text_image_section'];
		$cta_section 						=  $page_fields['cta_section'];
		$reviews_section 				=  $page_fields['reviews_section'];
		$amazon_images_slider 	=  $page_fields['amazon_images_slider'];
		$intro_secion 						=  $page_fields['intro_secion'];
	?>
	<!-------------------Banner Section Start---------------->
	<?php if($banner_section['enable_banner']):
		$banner_bg									=	$banner_section['banner_bg'];
		$banner_image							=	$banner_section['banner_image'];
		$banner_text								=	$banner_section['banner_text'];
	?>
		<div class="banner-section" <?php if($banner_bg): echo 'style="background:#000 url('.$banner_bg['url'].') no-repeat center;"'; endif;?>>
			<div class="container section">
				<div class="left-section">
					<?php if($banner_image):?>
						<img src="<?php echo $banner_image['url'];?>" alt="<?php echo $banner_image['alt'];?>"/>
					<?php endif; ?>
				</div>
				<div class="right-section">
					<?php if($banner_text):
						echo $banner_text;
					endif; ?>
				</div>
			</div>
		</div>
	<?php 
	endif;
	?>
	<!-------------------Banner Section End----------------->
	<!-------------------Page Content Section Start---------------->
		<?php 
		$post							= get_post($page_id);
		$content 					= $post->post_content;
			if($content){
				$content 			= apply_filters('the_content', $content);
				$content 			= str_replace(']]>', ']]>', $content);
			?>
			<div class="wrapper-boxed type-page">
				<div class="container">
						<?php echo $content;?>
				</div>
			</div>
		<?php } ?>	
	<!-------------------Page Content Section End---------------->
	<!-------------------Content Section Start---------------->
	<?php if($content_section['enable_content_section']):
		$content_bg_color					=	$content_section['content_bg_color'];
		$add_content							=	$content_section['add_content'];
			if($add_content):
	?>
			<div class="custom-content-section wrapper-boxed" <?php if($content_bg_color): echo 'style="background:'.$content_bg_color.'"'; endif;?>>
				<div class="container">
					<?php echo $add_content;?>
				</div>
			</div>
	<?php
		endif;
	endif;
	?>
	<!-------------------Content Section End----------------->
	<!-------------------Text/Image Section Start---------------->
	<?php if($text_image_section['enable_text_image_section']):
		$bg_color									=	$text_image_section['bg_color'];
		$text_section							=	$text_image_section['text_section'];
		$image_section						=	$text_image_section['image_section'];
	?>
		<div class="text-image-section  section" <?php if($bg_color): echo 'style="background:'.$bg_color.'"'; endif;?>>
			<div class="left-section">
				<?php if($text_section):
					echo $text_section;
				endif; ?>
			</div>
			<div class="right-section">
				<?php if($image_section):?>
					<img src="<?php echo $image_section['url'];?>" alt="<?php echo $image_section['alt'];?>"/>
				<?php endif; ?>
			</div>
		</div>
	<?php
	endif;
	?>
	<!-------------------Text/Image Section End----------------->
	<!-------------------CTA Section Start---------------->
	<?php if($cta_section['enable_cta_section']):
		$cta_bg_color							=	$cta_section['cta_bg_color'];
		$cta_content							=	$cta_section['cta_content'];
			if($cta_content):
	?>
			<div class="cta-section wrapper-boxed" <?php if($cta_bg_color): echo 'style="background:'.$cta_bg_color.'"'; endif;?>>
				<div class="container">
					<?php echo $cta_content;?>
				</div>
			</div>
	<?php
		endif;
	endif;
	?>
	<!-------------------CTA Section End----------------->
	<!-------------------Reviews Section Start---------------->
	<?php if($reviews_section['enable_reviews_section']):
		$reviews_heading					=	$reviews_section['reviews_heading'];
		$add_reviews							=	$reviews_section['add_reviews'];
			if($add_reviews):
	?>
			<div class="reviews-section slider-section wrapper-boxed">
				<div class="container">
				<?php if($reviews_heading):
					echo '<h3>'.$reviews_heading.'</h3>';
				endif; ?>
					<div class="owl-carousel owl-theme">
					<?php 
						foreach($add_reviews as $add_review):
							$review_text 		=	$add_review['review_text'];
							$review_author 	=	$add_review['review_author'];
					?>
						<div class="single-review">
							<div class="review-text"><p>“<?php echo $review_text;?>”</p></div>
							<p class="review-author">– <?php echo $review_author;?></p>
						</div>
					<?php endforeach;?>
					</div>
				</div>
			</div>
	<?php
		endif;
	endif;
	?>
	<!-------------------Reviews Section End----------------->
	<!-------------------Amazon Images Section Start---------------->
	<?php if($amazon_images_slider['enable_amazon_slider']):
		$bg_color								=	$amazon_images_slider['bg_color'];
		$add_images						=	$amazon_images_slider['add_images'];
			if($add_images):
	?>
			<div class="amazon-section  slider-section wrapper-boxed" <?php if($bg_color): echo 'style="background:'.$bg_color.'"'; endif;?>>
				<div class="container">
					<div class="owl-carousel owl-theme">
						<?php 
							foreach($add_images as $add_image):
								$select_image 		=	$add_image['select_image'];
						?>
							<div class="single-image">
								<a href="<?php echo $select_image['url'];?>" target="_blank"><img src="<?php echo $select_image['url'];?>" alt="<?php echo $select_image['alt'];?>"/></a>
							</div>
						<?php endforeach;?>
					</div>
				</div>
			</div>
	<?php
		endif;
	endif;
	?>
	<!-------------------Amazon Images Section End----------------->
	<!-------------------Intro Section Start---------------->
	<?php if($intro_secion['enable_intro_section']):
		$intro_bg								=	$intro_secion['intro_bg'];
		$intro_text							=	$intro_secion['intro_text'];
	?>
		<div class="intro-section wrapper-boxed" <?php if($intro_bg): echo 'style="background:#000 url('.$intro_bg['url'].') no-repeat center;"'; endif;?>>
			<div class="container section">
				<div class="right-section">
					<?php if($intro_text):
						echo $intro_text;
					endif; ?>
				</div>
			</div>
		</div>
	<?php
	endif;
	?>
	<!-------------------Intro Section End----------------->
<?php get_footer(); ?>