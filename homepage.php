<?php
/**
 * Template Name: Home Page
 *
 * @package Wordpress
 * @since   2.3
 */
get_header();

$breadcrumb = get_post_meta( get_the_ID(), 'penci_page_breadcrumb', true );

$featured_boxes = get_post_meta( get_the_ID(), 'penci_page_display_featured_boxes', true );

$page_meta = get_post_meta( get_the_ID(), 'penci_page_slider', true );

$rev_shortcode = get_post_meta( get_the_ID(), 'penci_page_rev_shortcode', true );



if( in_array( $page_meta, array('style-1', 'style-2', 'style-3', 'style-4', 'style-5', 'style-6', 'style-7', 'style-8', 'style-9', 'style-10', 'style-11', 'style-12', 'style-13', 'style-14', 'style-15', 'style-16', 'style-17', 'style-18', 'style-19', 'style-20', 'style-21', 'style-22', 'style-23', 'style-24', 'style-25',	'style-26', 'style-27', 'style-28', 'style-29', 'style-30',	'style-31', 'style-32', 'style-33', 'style-34', 'style-35', 'style-36', 'style-37', 'style-38', 'video' ) ) ) {

	if( $page_meta == 'video' && get_theme_mod( 'penci_featured_video_url' ) ) {

		get_template_part( 'inc/featured_slider/featured_video' );

	} else {

		if( ( $page_meta == 'style-33' || $page_meta == 'style-34' ) && $rev_shortcode ) {

			echo '<div class="featured-area featured-' . $page_meta . '">';

			if( $page_meta == 'style-34' ): echo '<div class="container">'; endif;

			echo do_shortcode( $rev_shortcode );

			if( $page_meta == 'style-34' ): echo '</div>'; endif;

			echo '</div>';

		} else {

			if ( get_theme_mod( 'penci_body_boxed_layout' ) && ! get_theme_mod( 'penci_vertical_nav_show' ) ) {

				if( $page_meta == 'style-3' ) {

					$page_meta == 'style-1';

				} elseif( $page_meta == 'style-5' ) {

					$page_meta == 'style-4';

				} elseif( $page_meta == 'style-7' ) {

					$page_meta == 'style-8';

				} elseif( $page_meta == 'style-9' ) {

					$page_meta == 'style-10';

				} elseif( $page_meta == 'style-11' ) {

					$page_meta == 'style-12';

				} elseif( $page_meta == 'style-13' ) {

					$page_meta == 'style-14';

				} elseif( $page_meta == 'style-15' ) {

					$page_meta == 'style-16';

				} elseif( $page_meta == 'style-17' ) {

					$page_meta == 'style-18';

				} elseif( $page_meta == 'style-29' ) {

					$page_meta == 'style-30';

				} elseif( $page_meta == 'style-35' ) {

					$page_meta == 'style-36';

				}

			}

			$slider_class = $page_meta;

			if( $page_meta == 'style-5' ) {

				$slider_class = 'style-4 style-5';

			} elseif ( $page_meta == 'style-30' ) {

				$slider_class = 'style-29 style-30';

			} elseif ( $page_meta == 'style-36' ) {

				$slider_class = 'style-35 style-36';

			}

			$data_auto = 'false';

			$data_loop = 'true';

			$data_res = '';



			if( $page_meta == 'style-7' || $page_meta == 'style-8' ){

				$data_res = ' data-item="4" data-desktop="4" data-tablet="2" data-tabsmall="1"';

			} elseif( $page_meta == 'style-9' || $page_meta == 'style-10' ){

				$data_res = ' data-item="3" data-desktop="3" data-tablet="2" data-tabsmall="1"';

			} elseif( $page_meta == 'style-11' || $page_meta == 'style-12' ){

				$data_res = ' data-item="2" data-desktop="2" data-tablet="2" data-tabsmall="1"';

			} elseif( in_array( $page_meta, array( 'style-31', 'style-32', 'style-35', 'style-36', 'style-37' ) ) ) {

				$data_next_prev = get_theme_mod( 'penci_enable_next_prev_penci_slider' ) ? 'true' : 'false';

				$data_dots = get_theme_mod( 'penci_disable_dots_penci_slider' ) ? 'false' : 'true';

				$data_res = ' data-dots="'. $data_dots .'" data-nav="'. $data_next_prev .'"';

			}



			if( get_theme_mod( 'penci_featured_autoplay' ) ): $data_auto = 'true'; endif;

			if( get_theme_mod( 'penci_featured_loop' ) ): $data_loop = 'false'; endif;

			$auto_time = get_theme_mod( 'penci_featured_slider_auto_time' );

			if( !is_numeric( $auto_time ) ): $auto_time = '4000'; endif;

			$auto_speed = get_theme_mod( 'penci_featured_slider_auto_speed' );

			if( !is_numeric( $auto_speed ) ): $auto_speed = '600'; endif;

			$open_container = '';

			$close_container = '';

			if( in_array( $page_meta, array( 'style-1', 'style-4', 'style-6', 'style-8', 'style-10', 'style-12', 'style-14', 'style-16', 'style-18', 'style-19', 'style-20', 'style-21', 'style-22', 'style-23', 'style-24', 'style-25', 'style-26', 'style-27', 'style-30', 'style-32', 'style-36', 'style-37' ) ) ):

				$open_container = '<div class="container">';

				$close_container = '</div>';

			endif;

			

			if( get_theme_mod( 'penci_enable_flat_overlay' ) && in_array( $page_meta, array( 'style-6', 'style-7', 'style-8', 'style-9', 'style-10', 'style-11', 'style-12', 'style-13', 'style-14', 'style-15', 'style-16', 'style-17', 'style-18', 'style-19', 'style-20', 'style-21', 'style-22', 'style-23', 'style-24', 'style-25', 'style-26', 'style-27', 'style-28' ) ) ): $slider_class .= ' penci-flat-overlay'; endif;



			echo '<div class="featured-area featured-' . $slider_class . '">' . $open_container;

			if( $page_meta == 'style-37' ):

				echo '<div class="penci-featured-items-left">';

			endif;

			echo '<div class="penci-owl-carousel penci-owl-featured-area"'. $data_res .' data-style="'. $page_meta .'" data-auto="'. $data_auto .'" data-autotime="'. $auto_time .'" data-speed="'. $auto_speed .'" data-loop="'. $data_loop .'">';

			get_template_part( 'inc/featured_slider/' . $page_meta );

			echo '</div>';

			echo $close_container. '</div>';

		}

	}

}



/* Display Featured Boxes */

if ( $featured_boxes == 'yes' && ! get_theme_mod( 'penci_home_hide_boxes' ) && ( get_theme_mod( 'penci_home_box_img1' ) || get_theme_mod( 'penci_home_box_img2' ) || get_theme_mod( 'penci_home_box_img3' ) || get_theme_mod( 'penci_home_box_img4' ) ) ):

	get_template_part( 'inc/modules/home_boxes' );

endif;



?>

<?php $show_page_title = penci_is_pageheader(); ?>

<?php if( ! $show_page_title ): ?>

	<?php if ( ! get_theme_mod( 'penci_disable_breadcrumb' ) && ( 'no' != $breadcrumb ) ): ?>

		<?php

		$yoast_breadcrumb = '';

		if ( function_exists( 'yoast_breadcrumb' ) ) {

			$yoast_breadcrumb = yoast_breadcrumb( '<div class="container container-single-page penci-breadcrumb">', '</div>', false );

		}



		if( $yoast_breadcrumb ){

			echo $yoast_breadcrumb;

		}else{ ?>

		<div class="container container-single-page penci-breadcrumb">

			<span><a class="crumb" href="<?php echo esc_url( home_url('/') ); ?>"><?php echo penci_get_setting( 'penci_trans_home' ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>

			<?php

			$page_parent = get_post_ancestors( get_the_ID() );

			if( ! empty( $page_parent ) ):

				$page_parent = array_reverse($page_parent);

				foreach( $page_parent as $pages ){

			?>

				<span><a class="crumb" href="<?php echo get_permalink( $pages ); ?>"><?php echo get_the_title( $pages ); ?></a></span><?php penci_fawesome_icon('fas fa-angle-right'); ?>

			<?php }

			endif; ?>

			<span><?php the_title(); ?></span>

		</div>

		<?php } ?>

	<?php endif; ?>

<?php endif; ?>



	<div class="template-homepage container-single-page penci_is_nosidebar container-single-page-full-width">
	<?php 
		$page_id 								=	get_the_ID(); 
		$page_fields  						=  get_fields($page_id);
		$banner_section  				=	$page_fields['homepage_banner_section'];
		$brands_section  				=  $page_fields['brands_section'];
		$enable_social_media  		=  $page_fields['enable_social_media'];
		$about_section  					=  $page_fields['about_section'];
		$books_section 					=  $page_fields['books_section'];
		$training_section 				=  $page_fields['personal_training_section'];
		$blog_section 						=  $page_fields['blog_section'];
		$newsletter_section 			=  $page_fields['newsletter_section'];
	?>
	<!-------------------Banner Section Start---------------->
	<?php if($banner_section['enable_banner_section']):
		$banner_image							=	$banner_section['add_banner_image'];
		$banner_image_mbl					=	$banner_section['add_banner_mobile_image'];
		$banner_link								=	$banner_section['add_banner_link'];
	?>
		<div class="banner-section">
			<div class="section">
					<?php if($banner_link):?><a href="<?php echo $banner_link;?>"><?php endif; ?>
					<?php if(wp_is_mobile()): ?>
						<img src="<?php echo $banner_image_mbl['url'];?>" alt="<?php echo $banner_image_mbl['alt'];?>"/>
					<?php else: ?>
						<img src="<?php echo $banner_image['url'];?>" alt="<?php echo $banner_image['alt'];?>"/>
					<?php endif; ?>
					<?php if($banner_link):?></a><?php endif; ?>
			</div>
		</div>
	<?php 
	endif;
	?>
	<!-------------------Banner Section End----------------->
	<!-------------------Social Media Section Start---------------->
	<?php if($enable_social_media && ! get_theme_mod( 'penci_header_social_check' ) ):?>
		<div class="header-social<?php if ( get_theme_mod( 'penci_header_social_brand' ) ): echo ' penci-social-textcolored'; endif; ?>">
			<?php get_template_part( 'inc/modules/socials' ); ?>
		</div>
	<?php endif; ?>
	<!-------------------Social Media Section End---------------->
	<!-------------------Page Content Section Start---------------->
		<?php 
		$post							= get_post($page_id);
		$content 					= $post->post_content;
			if($content){
				$content 			= apply_filters('the_content', $content);
				$content 			= str_replace(']]>', ']]>', $content);
			?>
			<div class="wrapper-boxed type-page page-content">
				<div class="container">
						<?php echo $content;?>
				</div>
			</div>
		<?php } ?>	
	<!-------------------Page Content Section End---------------->
	<!-------------------Brands Section Start---------------->
	<?php if($brands_section['enable_brands_section']):
		$brands_section_heading					=	$brands_section['brands_section_heading'];
		$brand_section_bg_color					=	$brands_section['brand_section_bg_color'];	
		$add_brands										=	$brands_section['add_brands'];	
	?>
			<div class="brands-section wrapper-boxed" <?php if($brand_section_bg_color): echo 'style="background:'.$brand_section_bg_color.'"'; endif;?>>
				<div class="container">
				<?php if($brands_section_heading):
					echo '<h2 class="section-heading">'.$brands_section_heading.'</h2>';
				endif; ?>
				<div class="brands">
					<?php 
						foreach($add_brands as $add_brand):
							$add_brand_logo 				=	$add_brand['add_brand_logo'];
							$add_brand_link 					=	$add_brand['add_brand_link'];
					?>
						<div class="single-brand">
							<a href="<?php echo $add_brand_link['url'];?>" target="<?php echo $add_brand_link['target'];?>">
								<img src="<?php echo $add_brand_logo['url'];?>" alt="<?php echo $add_brand_logo['alt'];?>"/>
							</a>
						</div>
					<?php endforeach;?>
					</div>
				</div>
			</div>
	<?php
	endif;
	?>
	<!-------------------Brands Section End----------------->
	<!-------------------About Section Start---------------->
	<?php if($about_section['enable_about_section']):
		$about_section_heading				=	$about_section['about_section_heading'];
		$text_section									=	$about_section['about_content'];
		$image_section								=	$about_section['about_section_image'];
		$about_section_button_label		=	$about_section['about_section_button_label'];
		$about_section_button_link  		=	$about_section['about_section_button_link'];
	?>
		<div class="text-image-section  about-section">
			<div class="container">
				<div class="left-section">
					<?php if($about_section_heading):?><h2 class="heading"><?php echo $about_section_heading;?></h2><?php endif; ?>
					<?php if($text_section):
						echo $text_section;
					endif; ?>
					<?php if($about_section_button_label):?>
						<a class="link-btn" href="<?php echo $about_section_button_link;?>"><?php echo $about_section_button_label;?> <i class="fa fa-angle-double-right"></i></a>
					<?php endif; ?>
				</div>
				<div class="right-section">
					<?php if($image_section):?>
						<img src="<?php echo $image_section['url'];?>" alt="<?php echo $image_section['alt'];?>"/>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php
	endif;
	?>
	<!-------------------About Section End----------------->
	<!-------------------Books Section Start---------------->
	<?php if($books_section['enable_books_section']):
		$books_section_heading					=	$books_section['books_section_heading'];
		$books_section_bg_color					=	$books_section['books_section_bg_color'];	
		$books_section_content					=	$books_section['books_section_content'];	
		$add_books											=	$books_section['add_books'];	
	?>
			<div class="books-section wrapper-boxed section" <?php if($books_section_bg_color): echo 'style="background:'.$books_section_bg_color.'"'; endif;?>>
				<div class="container">
				<?php if($books_section_heading):
					echo '<h2 class="section-heading">'.$books_section_heading.'</h2>';
				endif; ?>
				<?php if($books_section_content):
					echo '<div class="section-content">'.$books_section_content.'</div>';
				endif; ?>
				<?php 
					foreach($add_books as $add_book):
						$add_book_image 				=	$add_book['add_book_image'];
						$add_book_title 					=	$add_book['add_book_title'];
						$add_book_desc 				=	$add_book['add_book_desc'];
						$select_book_page_link 	=	$add_book['select_book_page_link'];
				?>
					<div class="single-book">
						<div class="book-img">
							<span>
								<img src="<?php echo $add_book_image['url'];?>" alt="<?php echo $add_book_image['alt'];?>"/>
								<a href="<?php echo $select_book_page_link['url'];?>" target="<?php echo $select_book_page_link['target'];?>" class="link-btn">Buy Here <i class="fa fa-angle-double-right"></i></a>
							</span>
						</div>
						<div class="book-info">
							<h3><a href="<?php echo $select_book_page_link['url'];?>" target="<?php echo $select_book_page_link['target'];?>" ><?php echo $add_book_title;?></a></h3>
							<?php if($add_book_desc):
								echo $add_book_desc;
							endif; ?>
						</div>
					</div>
				<?php endforeach;?>
				</div>
			</div>
	<?php
	endif;
	?>
	<!-------------------Books Section End----------------->
	<!-------------------Training Section Start---------------->
	<?php if($training_section['enable_personal_training_section']):
		$personal_training_heading		=	$training_section['personal_training_heading'];
		$training_section_bg_color		=	$training_section['training_section_bg_color'];
		$left_side_content						=	$training_section['left_side_content'];
		$training_types							=	$training_section['training_types'];
		$button_1_label  						=	$training_section['training_section_button_1_label'];
		$button_1_link  							=	$training_section['training_section_button_1_link'];
		$button_2_label  						=	$training_section['training_section_button_2_label'];
		$button_2_link  							=	$training_section['training_section_button_2_link'];
	?>
		<div class="training-section section" <?php if($training_section_bg_color): echo 'style="background:'.$training_section_bg_color.'"'; endif;?>>
			<div class="container">
				<?php if($personal_training_heading):
					echo '<h2 class="section-heading">'.$personal_training_heading.'</h2>';
				endif; ?>
				<div class="text-image-section">
					<div class="left-section">
						<?php if($left_side_content):
							echo $left_side_content;
						endif; ?>
					</div>
					<div class="right-section">
						<?php if($training_types):
							foreach($training_types as $training_type):
							$training_heading 			=	$training_type['training_heading'];
							$training_desc 				=	$training_type['training_desc'];
							$training_link 					=	$training_type['training_link'];
						?>
							<div class="single-type">
							<?php if($training_link):?>
								<a href="<?php echo $training_link['url'];?>" target="<?php echo $training_link['target'];?>" >
							<?php endif; ?>
							<h4><?php echo $training_heading;?></h4>
							<?php if($training_desc):
								echo $training_desc;
							endif; ?>
							<?php if($training_link):?>
								</a>
							<?php endif; ?>
						</div>
						<?php 
							endforeach;
						endif; ?>
					</div>
				</div>
				<?php if($button_1_label || $button_2_label ):?>
					<div class="action-buttons">
						<?php if($button_1_label):?>
							<a class="link-btn" href="<?php echo $button_1_link['url'];?>" target="<?php echo $button_1_link['target'];?>" ><?php echo $button_1_label;?> <i class="fa fa-angle-double-right"></i></a>
						<?php endif; ?>
						<?php if($button_2_label):?>
							<a class="link-btn" href="<?php echo $button_2_link['url'];?>"  target="<?php echo $button_2_link['target'];?>" ><?php echo $button_2_label;?> <i class="fa fa-angle-double-right"></i></a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php
	endif;
	?>
	<!-------------------Training Section End----------------->
	<!-------------------Blog Section Start---------------->
	<?php if($blog_section['enable_blog_section']):
		$blog_section_heading						=	$blog_section['blog_section_heading'];
		$blog_section_content						=	$blog_section['blog_section_content'];	
		$blog_page_link									=	$blog_section['blog_page_link'];	
		$add_books											=	$blog_section['add_books'];	
	?>
			<div class="blog-section wrapper-boxed section">
				<div class="container">
				<?php if($blog_section_heading):
					echo '<h2 class="section-heading">'.$blog_section_heading.'</h2>';
				endif; ?>
				<?php if($blog_section_content):
					echo '<div class="section-content">'.$blog_section_content.'</div>';
				endif; ?>
				<?php 
					if(wp_is_mobile()):
						$blog_args = array(
							'post_type' 					=> 'post',
							'posts_per_page'			=> 3,
							'post_status' 				=> 'publish'
						);
					else:
						$blog_args = array(
							'post_type' 					=> 'post',
							'posts_per_page'			=> 9,
							'post_status' 				=> 'publish'
						);
					endif;
					$blogs = wp_get_recent_posts( $blog_args );?>
					<div class="penci-wrapper-posts-content">
						<!--<ul class="penci-wrapper-data penci-grid owl-carousel owl-theme">-->
						<ul class="penci-wrapper-data penci-grid">
							<?php
							foreach($blogs as $blog):
								$blog_id			=	$blog['ID'];  
								include( locate_template( 'content-grid-home.php', false, false ) );
							endforeach;?>
						</ul>
					</div>
					<?php if($blog_page_link):?>
						<div class="action-buttons">
							<a class="link-btn" href="<?php echo $blog_page_link['url'];?>"  target="<?php echo $blog_page_link['target'];?>" >View All <i class="fa fa-angle-double-right"></i></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
	<?php
	endif;
	?>
	<!-------------------Blog Section End----------------->
	<!-------------------Newsletter Section Start---------------->
	<?php if($newsletter_section['enable_newsletter_section']):
		$newsletter_section_heading	=	$newsletter_section['newsletter_section_heading'];
		$newsletter_section_bg_color	=	$newsletter_section['newsletter_section_bg_color'];
		$newsletter_content					=	$newsletter_section['newsletter_content'];
		$newsletter_form_shortcode   =	$newsletter_section['newsletter_form_shortcode'];
	?>
		<div class="newsletter-section section" <?php if($newsletter_section_bg_color): echo 'style="background:'.$newsletter_section_bg_color.'"'; endif;?>>
			<div class="container text-image-section">
				<div class="left-section">
					<?php if($newsletter_section_heading):
						echo '<h2 class="section-heading">'.$newsletter_section_heading.'</h2>';
					endif; ?>
					<?php if($newsletter_content):
						echo $newsletter_content;
					endif; ?>
				</div>
				<div class="right-section">
					<?php if($newsletter_form_shortcode):
						echo do_shortcode($newsletter_form_shortcode);
					endif; ?>
				</div>				
			</div>
		</div>
	<?php
	endif;
	?>
	<!-------------------Newsletter Section End----------------->
<?php get_footer(); ?>